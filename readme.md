# readme

## csv形式について
- utf8でおk！

|日付|氏名|案件番号|工数|備考|
|---|---|---|---|---|
|2018/5/19|アイウエ　太郎|106840|3|なし|

## TODO:
- firebase functions でメールを経由しエラーを検出
- win/macでのpackage方法
- 管理画面で編集
- 管理画面から日付ごとの詳細を見れる

## electron sqlite3 問題
- [Windows 上で Electron + sqlite3 のアプリを動かそうとするとエラーになる場合の対処法 - Qiita](https://qiita.com/noobar/items/0128677c44bb9dde88b2)
    - sqliteとelectronのやつ

```bash
node-gyp configure --module_name=node_sqlite3 --module_path=../lib/binding/electron-v2.0-darwin-x64
node-gyp rebuild --target=2.0.5 --arch=x64 --target_platform=darwin --dist-url=https://atom.io/download/atom-shell --module_name=node_sqlite3 --module_path=../lib/binding/electron-v2.0-darwin-x64
```