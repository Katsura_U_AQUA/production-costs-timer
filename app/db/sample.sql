create table if not exists product (
    id integer primary key autoincrement,
    name text not null,
    number text,
    is_active int not null default 1,
    created_at text default (date('now', 'localtime'))
);

create table if not exists product_time (
    id integer primary key autoincrement,
    date text default (date('now', 'localtime')),
    product_id int not null,
    count int not null default 0
);

-- とりあえず追加
    update product set is_active = 0;
    insert into product(name) values('案件1');
    insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);
    insert into product_time(product_id, date, count) values((select id from product where is_active = 1), '2018-07-18', 400);

    update product set is_active = 0;
    insert into product(name) values('案件2');
    insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

    update product set is_active = 0;
    insert into product(name) values('案件3');
    insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

    update product set is_active = 0;
    insert into product(name) values('案件4');
    insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

    update product set is_active = 0;
    insert into product(name) values('案件5');
    insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

    update product set is_active = 0;
    insert into product(name) values('案件6');
    insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

    update product set is_active = 0;
    insert into product(name) values('案件7');
    insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

    update product set is_active = 0;
    insert into product(name) values('案件8');
    insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

    update product set is_active = 0;
    insert into product(name) values('案件9');
    insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

    update product set is_active = 0;
    insert into product(name) values('案件10');
    insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

    update product set is_active = 0;
    insert into product(name) values('案件11');
    insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);


-- 作業を追加する時
update product set is_active = 0;
insert into product(name, number) values('hello', '123445');
insert into product(name) values('hello');
insert into product_time(product_id, count) values((select id from product where is_active = 1), 0);
select p.name, sum(t.count) from product p inner join product_time t on p.id = t.product_id where p.is_active = 1;

-- 作業を更新
update product_time set count = {} where product_id = i;

-- 起動時にデータ読み込み
select p.id, p.name, t.date, sum(t.count) as count from product p inner join product_time t on p.id = t.product_id where p.is_active = 1;

-- 起動時にデータ読み込み(日付別対応)
select p.id, p.name, t.date, sum(t.count) as old_count,
    (select count from product_time where date = date('now', 'localtime')) as count
    from product p
    inner join product_time t on p.id = t.product_id
    where p.is_active = 1 and t.date = date('now', 'localtime');

-- アクティブと今日の日付が一緒か確認
-- 引数:



-- 管理画面/CSV(まとめる)
select p.id, p.number, sum(t.count) as count from product p
    inner join product_time t on p.id = t.product_id
    where t.date between '2018-07-15' and date('now', 'localtime')
    group by p.id;

-- 管理画面/CSV(まとめない)
select p.id, p.number, t.count, t.date from product p
    inner join product_time t on p.id = t.product_id
    where t.date between '2018-07-15' and '2018-08-41';

-- 管理画面/一覧読み込み
select p.id, p.name, p.number, sum(t.count), p.created_at, max(t.date) as updated_at, p.is_active
    from product p
    inner join product_time t on p.id = t.product_id
    group by p.id
    order by updated_at desc
    ;

-- 作業を切り替える時
update product set is_active = 0;
update product set is_active = 1 where id = 1; -- 1 <- i
select * from product where id = 1; -- 1 <- i

-- 期間内の作業を取るとき(未使用予定)
select p.id, p.name, p.number, sum(t.count), p.created_at, max(t.date), p.is_active
    from product p
    inner join product_time t on p.id = t.product_id
    where t.date between '2018-07-15' and '2018-08-01'
    group by p.id;

-- 項目を編集(未使用予定)
update product set name = ${value} where id = ${id}
update product set number = ${value} where id = ${id}

-- 作業を削除
delete from product_time where product_id = 1; -- 1 <- i
delete from product where id = 1; -- 1 <- i

-- case when文
select
    case
        when p.created_at > "2018-08-01" then (select date from product_time where product_id = p.id)
        else "no"
    end
from product p;