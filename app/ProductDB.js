const sqlite = require("sqlite3")

const { getDateStr, toDateStrFormat } = require("./date-util")

const querySQLs = {
  create: /*sql*/`
    create table if not exists product (
        id integer primary key autoincrement,
        name text not null,
        number text,
        is_active int not null default 1,
        created_at text default (date('now', 'localtime'))
    );

    create table if not exists product_time (
        id integer primary key autoincrement,
        date text default (date('now', 'localtime')),
        product_id int not null,
        count int not null default 0
    );`,
    debug: /*sql*/`
      update product set is_active = 0;
      insert into product(name) values('案件1');
      insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);
      insert into product_time(product_id, date, count) values((select id from product where is_active = 1), '2018-07-18', 400);

      update product set is_active = 0;
      insert into product(name) values('案件2');
      insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

      update product set is_active = 0;
      insert into product(name) values('案件3');
      insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

      update product set is_active = 0;
      insert into product(name) values('案件4');
      insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

      update product set is_active = 0;
      insert into product(name) values('案件5');
      insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

      update product set is_active = 0;
      insert into product(name) values('案件6');
      insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

      update product set is_active = 0;
      insert into product(name) values('案件7');
      insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

      update product set is_active = 0;
      insert into product(name) values('案件8');
      insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

      update product set is_active = 0;
      insert into product(name) values('案件9');
      insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

      update product set is_active = 0;
      insert into product(name) values('案件10');
      insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);

      update product set is_active = 0;
      insert into product(name) values('案件11');
      insert into product_time(product_id, count) values((select id from product where is_active = 1), 200);
    `
}

const isNone = a => typeof a === "undefined" || a === null

const query = (db, method, query) => new Promise((resolve, reject) =>
  db[method](query, (e, row) => e ? reject(e) : resolve(row))
)

module.exports = class ProductDB {
  constructor(path, debug) {
    this.db = new sqlite.Database(path)
    this.db.serialize()
    this.db.exec(querySQLs.create)
    this.query = query.bind(null, this.db)

    if (debug) {
      this.db.exec(querySQLs.debug)
    }
  }

  _getActiveProduct() {
    return this.query("get",/*sql*/`
        select t.id, p.id as product_id, p.name, t.date,
          (select count from product_time where p.id = product_id and date = date('now', 'localtime')) as count,
          sum(t.count) as old_count
          from product p
          inner join product_time t on p.id = t.product_id
          where p.is_active = 1 ;`
      )
      .then(data => {
        if (data.product_id === null)
          return null
        else if (!isNone(data.count)) {
          data.old_count -= data.count
          return data
        }
        else {
          return this._insertProductTimeDate(data)
        }
      })
  }

  _insertProductTimeDate(data){
    return Promise.resolve()
      .then(() => this.query("exec", /*sql*/`
        insert into product_time(product_id) values(${data.product_id});`
      ))
      .then(() => this.query("get",  /*sql*/`
        select id, date, count from product_time
          where product_id = ${data.product_id} and date = date('now', 'localtime');`
      ))
      .then(row => Object.assign(data, row))
  }

  startUp() {
    return this._getActiveProduct()
  }

  add({name, number} = {}) {
    if (!name) throw new Error("'name' is not exist.")

    const q = number ? /*sql*/`insert into product(name, number) values('${name}', '${number}');`
                     : /*sql*/`insert into product(name) values('${name}');`

    return this.query("exec", /*sql*/`
      update product set is_active = 0;
      ${q}
      insert into product_time(product_id) values((select id from product where is_active = 1));`
    )
    .then(() => this._getActiveProduct())

  }

  updateCount({id, count, date}) {
    const todayStr = getDateStr(new Date())
    const q = (i, c) => this.query("exec",/*sql*/`update product_time set count = ${c} where id = ${i};`)
    if (date && date !== todayStr) {
      return this._getActiveProduct()
        .then(data => q(data.id, data.count = 1).then(() => data))
    }
    else {
      return q(id, count)
    }
  }

  update({id, target, value}) {
    return this.query("exec", /*sql*/`update product set ${target} = '${value}' where id = ${id};`)
  }

  csvData({start, end}, noSummary) {
    const commonSql = /*sql*/`
      from product p
      inner join product_time t on p.id = t.product_id
      where t.date between '${start}' and '${end}'`

    const sortSql = /*sql*/`order by t.date desc`

    if (noSummary) {
      return this.query("all", /*sql*/`select p.id, p.number, t.count, t.date ${commonSql} ${sortSql};`)
        .then(rows => {
          rows.forEach(row => row.date = toDateStrFormat(row.date))
          return rows
        })
    } else {
      return this.query("all", /*sql*/`select p.id, p.number, sum(t.count) as count ${commonSql} group by p.id ${sortSql};`)
        .then(rows => {
          rows.forEach(row => row.date = toDateStrFormat(getDateStr()))
          return rows
        })
    }
  }

  listData() {
    return this.query("all", /*sql*/`
      select p.id, p.name, p.number, sum(t.count) as count, p.created_at, max(t.date) as updated_at, p.is_active
        from product p
        inner join product_time t on p.id = t.product_id
        group by p.id
        order by updated_at desc;`
    ).then(rows => {
      rows.forEach(row => {
        row.updated_at = toDateStrFormat(row.updated_at)
        row.created_at = toDateStrFormat(row.created_at)
        row.is_active = !!row.is_active
      })
      return rows
    })
  }

  change({id}) {
    return this.query("exec", /*sql*/`
      update product set is_active = 0;
      update product set is_active = 1 where id = ${id};`
    )
    .then(() => this._getActiveProduct())
  }

  delete({id}) {
    return this.query("exec", /*sql*/`
      delete from product_time where product_id = ${id};
      delete from product where id = ${id};`
    )
  }

  close() {
    this.db.close()
  }
}