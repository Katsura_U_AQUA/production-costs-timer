const zeroUme = i => i < 10 ? "0"+i : i

module.exports.getDateStr = (date = new Date()) => `${date.getFullYear()}-${zeroUme(date.getMonth() + 1)}-${zeroUme(date.getDate())}`
module.exports.toDateStrFormat = dateStr => dateStr.replace(/-/g, "/")