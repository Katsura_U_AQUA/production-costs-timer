const Vue = require("../vue.min.js")
Vue.directive('focus', {
  inserted(el) {
    el.focus()
  }
})