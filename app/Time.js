module.exports = class Time {
  constructor(count, oldCount) {
    count = count || 0
    if (typeof count !== "number") throw new Error("'_time' is not number.")
    this.oldCount = oldCount || 0
    this.count = count
  }
  get time() {
    return this.oldCount + this.count
  }

  get hour() {
    return Math.floor(this.time / 60)
  }

  get minite() {
    return this.time % 60
  }

  getHourMinite() {
    return [this.hour, this.minite]
  }

  getProductTime() {
    return (this.time / 60).toFixed(1)
  }

  toString() {
    const minite = this.minite
    return `${this.hour}:${minite < 10 ? "0"+minite : minite}`
  }

}