const fs = require("fs")
const os = require("os")
const { join } = require("path")
const electron = require("electron")
const { app, BrowserWindow, ipcMain, Menu } = electron
const menubar = require("menubar")
const ProductDB = require("./ProductDB")
const icon = require("./icon")

// init
const HOME_PATH = os.homedir()
const APP_NAME = "production-costs-timer"
const APP_PATH = join(HOME_PATH, ".utsuhashi", APP_NAME)
const CONFIG_PATH = join(APP_PATH, "setting.json")
const DB_PATH = join(APP_PATH, "test.db")

const promiseTest = p => p.then(data => console.log("success! data:", data)).catch(e => console.log("err!", e))

const self = {
  get db() {
    if (!this._db) {
      this._db = new ProductDB(DB_PATH)
    }
    return this._db
  }
}

const readConfigPromise = () => new Promise((resolve, reject) =>
  self.config ? resolve(self.config) :
    fs.readFile(CONFIG_PATH, (e, buf) => e ? reject(e) : resolve(self.config = JSON.parse(buf.toString())))
)


const mb = menubar({
  dir: __dirname + "/renderer/",
  preloadWindow: true,
  tooltip: "工数タイマー",
  icon: icon("deactive"),
  width: 250,
  height: 200,
  resizable: false
})

const startUpPromise = new Promise(resolve => fs.exists(CONFIG_PATH, resolve))
  .then(result => result || require("./init"))
  .catch(console.log)
  .then(function ipcMainInit() {
    let managementWindow, settingWindow

    const openManagementWindow = () => {
      if (!managementWindow) {
        managementWindow = new BrowserWindow({
          width: 850,
          height: 500,
          title: "管理画面 - 工数タイマー",
          fullscreenable: false,
          resizable: false,
          show: false
        })
        managementWindow.setMenu(null)
        managementWindow.loadFile(`${__dirname}/renderer/management.html`)
        managementWindow.on("ready-to-show", () => managementWindow.show())
        managementWindow.on("close", () => managementWindow = null)
      } else {
        managementWindow.focus()
      }
    }
    const openSettingWindow = () => {
      if (!settingWindow) {
        settingWindow = new BrowserWindow({
          width: 400,
          height: 300,
          title: "設定 - 工数タイマー",
          fullscreenable: false,
          resizable: false,
          show: false
        })
        settingWindow.setMenu(null)
        const readyPromise = new Promise(resolve => settingWindow.on("ready-to-show", resolve))
        settingWindow.loadFile(`${__dirname}/renderer/setting.html`)
        settingWindow.on("close", () => settingWindow = null)

        Promise.all([readConfigPromise(), readyPromise]).then(([config]) => {
          settingWindow.webContents.send("configを読み込む時", config)
          settingWindow.show()
        })
      } else {
        settingWindow.focus()
      }
    }

    const menuDefaults = [
      { label: "管理", click: openManagementWindow },
      { label: "設定", accelerator: "cmd+,", click: openSettingWindow },
      { type: "separator" },
      {
        label: "終了", accelerator: "cmd+q", click: () => {
          self.db.close()
          mb.app.quit()
        }
      }
    ]

    ipcMain.on("メニュークリックした時", e => {
      const menu = Menu.buildFromTemplate(menuDefaults)
      menu.popup({ window: mb.window })
    })

    ipcMain.on('closeApp', (event, close) => {
      self.db.close()
      mb.app.quit()
    })

    ipcMain.on("window外をクリックした時", (e, option) => mb.tray.setImage(icon(option.isPlay ? "deactive" : "no-product")))

    ipcMain.on("1分ごとにカウントする時", (e, data) => {
      promiseTest(
        self.db.updateCount(data)
          .then(data => data && mb.window.webContents.send("productを受け取る時", data))
      )
    })

    ipcMain.on("追加した時", (event, data) => {
      console.log(data);
      promiseTest(
        self.db.add(data)
          .then(product => mb.window.webContents.send("productを受け取る時", product))
      )
    })

    ipcMain.on("csvDataを欲しがった時", (e, limite, noSummary) => {
      self.db.csvData(limite, noSummary).then(data => managementWindow.webContents.send("csvDataを代入させたい時", data))
    })

    ipcMain.on("listDataを欲しがった時", e => {
      self.db.listData().then(data => managementWindow.webContents.send("listDataを代入させたい時", data))
    })

    ipcMain.on("作業切り替え時", (e, data) => {
      self.db.change(data).then(product => {
        managementWindow.webContents.send("作業切り替え完了時")
        mb.window.webContents.send("productを受け取る時", product)
        mb.tray.setImage(icon("deactive"))
      })
    })

    ipcMain.on("作業削除時", (e, data) => {
      self.db.delete(data).then(product => {
        managementWindow.webContents.send("作業削除完了時")
        if (data.is_active) {
          mb.window.webContents.send("作業が消された時")
          mb.tray.setImage(icon("no-product"))
        }
      })
    })

    ipcMain.on("configを更新した時", (e, config) => {
      self.config = config
      mb.window.webContents.send("configを代入させたい時", config)
      fs.writeFile(CONFIG_PATH, JSON.stringify(config), { encoding: "utf-8" }, e => {
        if (e) {
          console.error(e)
        }
        else {
          console.log(config, "を更新")
          mb.window.webContents.send("configを投げる時", config)
        }
      })
    })

    ipcMain.on("managementWindowがconfigを欲しがった時", e => {
      readConfigPromise().then(config => managementWindow.webContents.send("configを投げる時", config))
    })

    ipcMain.on("mainWindowをopenした時", e => {
      const content = mb.window.webContents
      readConfigPromise().then(config => content.send("configを投げる時", config))
      self.db.startUp().then(product => product && content.send("productを受け取る時", product))
    })

    ipcMain.on("managementWindowでupdateをした時", (e, data) => {
      self.db.update(data).then(() => managementWindow.webContents.send("update完了した時"))
    })

  })

const readyPromise = new Promise(resolve => mb.on('ready', resolve))

Promise.all([startUpPromise, readyPromise]).then(function ready() {
  const mainContents = mb.window.webContents
  mb.window.setSkipTaskbar(true)
  mb.tray.on("click", () => {
    mb.window.setSize(250, 200, false)
    mb.tray.setImage(icon("active"))
  })

  electron.powerMonitor.on("suspend", e => mainContents.send("スリープに入った時", e))
  electron.powerMonitor.on("resume", e => mainContents.send("スリープが解除された時", e))

})

