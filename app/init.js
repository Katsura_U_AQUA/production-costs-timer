const path = require("path")
const { join } = path
const fs = require("fs")
const os = require("os")

const APP_NAME = "production-costs-timer"
const HOME_PATH = os.homedir()

const makeDirPromise = (...paths) => {
  return new Promise((resolve, reject) => {
    const path = join(...paths)
    fs.exists(path, exists => resolve([exists, path]))
  })
    .then(([exists, path]) => new Promise((resolve, reject) => {
      if (!exists) {
        fs.mkdir(path, e => e ? reject(e) : resolve(path))
      } else {
        resolve(path)
      }
    }))
}

module.exports = Promise.resolve()
  .then(() => makeDirPromise(HOME_PATH, ".utsuhashi"))
  .then(path => makeDirPromise(path, APP_NAME))
  .then(path => main(path))


function main(path) {
  const defaultConfig = {
    NAME: "",
    SLEEP_NOTIFICATION: true,
    NOTIFICATION_INTERVAL: "0"
  }

  const promise = new Promise((resolve, reject) =>
    fs.writeFile(join(path, "setting.json"), JSON.stringify(defaultConfig), { encoding: "utf-8" }, e => e ? reject(e) : resolve(path))
  )
  return promise
}