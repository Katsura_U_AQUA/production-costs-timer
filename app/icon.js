
const { join } = require("path")
const PATH = join(__dirname, "images")
const isWin = require("os").platform() === "win32"
const o = {
  white: join(PATH, "time-left-white.png"),
  black: join(PATH, "time-left-black.png"),
  warning: join(PATH, "time-left-warning.png"),
}

module.exports = function icon(type) {
  switch(type) {
    case "active": return o.white
    case "deactive": return isWin ? o.white : o.black
    case "no-product": return o.warning
  }
}